import todoReducers from "./todoReduces";

import { combineReducers } from "redux";

const rootReducer = combineReducers({
    todoReducers
});

export default rootReducer;