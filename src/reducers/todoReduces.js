const initialData = {
    list: []
}

const todoReducers = (state = initialData, actions) => {

    switch (actions.type) {
        case "ADD_TODO":
            const { id, data } = actions.payload;
            return {
                ...state,
                list: [
                    ...state.list,
                    {
                        id: id,
                        data: data
                    }
                ]
            }
        case "DELETE_TODO":
            const newList = state.list.filter((elem) => elem.id !== actions.id)
            return {
                ...state,
                list: newList
            }
        case "REMOVE_TODO":
            return {
                ...state,
                list: []
            }
        default: return state;
    }

}

export default todoReducers;