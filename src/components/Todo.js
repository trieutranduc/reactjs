import React, { useState } from "react";
import { addTodo, deleteTodo, removeTodo } from "../actions/index";
import { useDispatch, useSelector } from "react-redux";

const Todo = () => {

    const [inputData, setInputData] = useState('');
    const list = useSelector((state) => state.todoReducers.list);
    const dispatch = useDispatch();

    return (
        <>
            <div className='main-div'>
                <div className='child-div'>
                    <figure>
                        <figcaption>Add your list here ?</figcaption>
                    </figure>
                    <div className='addItems'>
                        <input type='text' placeholder=' Add items....'
                            value={inputData}
                            onChange={(ev) => setInputData(ev.target.value)} />
                        <button className='fa fa-plus add-btn'
                            onClick={() => dispatch(addTodo(inputData), setInputData(''))}
                        ></button>
                    </div>

                    <div className="showItems">
                        {
                            list.map((elem) => {
                                return (
                                    <div className="eachItems" key={elem.id} style={{ border: '1px solid red' }}>
                                        <h3>{elem.data}</h3>
                                        <i className="far fa-trash-alt add-btn" title="Delete item"
                                            onClick={() => dispatch(deleteTodo(elem.id))}></i>
                                    </div>
                                )
                            })
                        }

                    </div>

                </div>
                <div className="showItems">
                    <button className="btn effect04"
                        onClick={() => dispatch(removeTodo())}
                        data-sm-link-text="remove All">

                        <span>Check List</span>
                    </button>
                </div>
            </div>
        </>
    )
}

export default Todo;